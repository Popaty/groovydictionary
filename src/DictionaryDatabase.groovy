import groovy.sql.Sql

/**
 * Created by Maximiliano on 8/9/15 AD.
 */
class DictionaryDatabase {

    public connectionDB(){
        def sql = Sql.newInstance("jdbc:hsqldb:file:datasource/dictionary","SA", "","org.hsqldb.jdbcDriver")
        return sql
    }

    public createtable(){
        def sql = connectionDB()
//        def sql = Sql.newInstance("jdbc:hsqldb:file:datasource/dictionary","SA", "","org.hsqldb.jdbcDriver")
        sql.execute("""create TABLE WORDS(
                        id INT IDENTITY,
                        vocabulary VARCHAR(50)
                    )""")
    }

    public inserttodb(filename){
//        def sql = Sql.newInstance("jdbc:hsqldb:file:datasource/dictionary","SA", "","org.hsqldb.jdbcDriver")
        def sql = connectionDB()
        def line = []
        new File(filename).withReader { line = it.readLines() }
        for (int i = 0; i < line.size(); i++) {
            String currentword = line[i]
            sql.execute("""INSERT into WORDS (VOCABULARY) VALUES (?)""",currentword)
//            assert sql.updateCount == 1
        }
        sql.commit()
        sql.close()
    }

    public findWordLengthMoreThan5(){
//        def sql = Sql.newInstance("jdbc:hsqldb:file:datasource/dictionary","SA", "","org.hsqldb.jdbcDriver")
        def sql = connectionDB()
        def list = []
        sql.rows("SELECT * FROM words WHERE CHAR_LENGTH(vocabulary) > 5").each {row ->
            list += "${row.vocabulary}"
        }
//        println(list.size())
        return list.size()
    }

    public findOccurenceChar2(){
//        def sql = Sql.newInstance("jdbc:hsqldb:file:datasource/dictionary","SA", "","org.hsqldb.jdbcDriver")
        def sql = connectionDB()
        def list = []
                sql.rows("""select * from words
                where (char_length(vocabulary) - char_length(replace(vocabulary, 'a' ,'' ))) >= 2 or
                (char_length(vocabulary) - char_length(replace(vocabulary, 'b' ,'' ))) >= 2 or
                (char_length(vocabulary) - char_length(replace(vocabulary, 'c' ,'' ))) >= 2 or
                (char_length(vocabulary) - char_length(replace(vocabulary, 'd' ,'' ))) >= 2 or
                (char_length(vocabulary) - char_length(replace(vocabulary, 'e' ,'' ))) >= 2 or
                (char_length(vocabulary) - char_length(replace(vocabulary, 'f' ,'' ))) >= 2 or
                (char_length(vocabulary) - char_length(replace(vocabulary, 'g' ,'' ))) >= 2 or
                (char_length(vocabulary) - char_length(replace(vocabulary, 'h' ,'' ))) >= 2 or
                (char_length(vocabulary) - char_length(replace(vocabulary, 'i' ,'' ))) >= 2 or
                (char_length(vocabulary) - char_length(replace(vocabulary, 'j' ,'' ))) >= 2 or
                (char_length(vocabulary) - char_length(replace(vocabulary, 'k' ,'' ))) >= 2 or
                (char_length(vocabulary) - char_length(replace(vocabulary, 'l' ,'' ))) >= 2 or
                (char_length(vocabulary) - char_length(replace(vocabulary, 'm' ,'' ))) >= 2 or
                (char_length(vocabulary) - char_length(replace(vocabulary, 'n' ,'' ))) >= 2 or
                (char_length(vocabulary) - char_length(replace(vocabulary, 'o' ,'' ))) >= 2 or
                (char_length(vocabulary) - char_length(replace(vocabulary, 'p' ,'' ))) >= 2 or
                (char_length(vocabulary) - char_length(replace(vocabulary, 'q' ,'' ))) >= 2 or
                (char_length(vocabulary) - char_length(replace(vocabulary, 'r' ,'' ))) >= 2 or
                (char_length(vocabulary) - char_length(replace(vocabulary, 's' ,'' ))) >= 2 or
                (char_length(vocabulary) - char_length(replace(vocabulary, 't' ,'' ))) >= 2 or
                (char_length(vocabulary) - char_length(replace(vocabulary, 'u' ,'' ))) >= 2 or
                (char_length(vocabulary) - char_length(replace(vocabulary, 'v' ,'' ))) >= 2 or
                (char_length(vocabulary) - char_length(replace(vocabulary, 'w' ,'' ))) >= 2 or
                (char_length(vocabulary) - char_length(replace(vocabulary, 'x' ,'' ))) >= 2 or
                (char_length(vocabulary) - char_length(replace(vocabulary, 'y' ,'' ))) >= 2 or
                (char_length(vocabulary) - char_length(replace(vocabulary, 'z' ,'' ))) >= 2""").each {row ->
            list += "${row.vocabulary}"
        }
        return list.size()

    }

    public findFirstandLastChar(){
//        def sql = Sql.newInstance("jdbc:hsqldb:file:datasource/dictionary","SA", "","org.hsqldb.jdbcDriver")
        def sql = connectionDB()
        def list = []
        sql.rows("select * from words where left(vocabulary,1) = right(vocabulary,1)").each {row ->
            list += "${row.vocabulary}"
        }
        return list.size()
    }

    public updateUppercase(){
//        def sql = Sql.newInstance("jdbc:hsqldb:file:datasource/dictionary","SA", "","org.hsqldb.jdbcDriver")
        def sql = connectionDB()
        sql.execute("UPDATE words set vocabulary = upper(left(vocabulary,1))+lower(substring(vocabulary,2,CHAR_LENGTH(vocabulary)))")
        sql.commit()
        sql.close()

    }

    public deleteDictionary(){
//        def sql = Sql.newInstance("jdbc:hsqldb:file:datasource/dictionary","SA", "","org.hsqldb.jdbcDriver")
//        sql.execute("DELETE FROM WORDS")
        def sql = connectionDB()
        sql.execute("DROP SCHEMA PUBLIC CASCADE")
        sql.commit()
        sql.close()
    }
}

