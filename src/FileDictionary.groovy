import groovy.io.FileType

/**
 * Created by Maximiliano on 8/9/15 AD.
 */
class FileDictionary {

    public createFile(filename){
        //get words to arraylist
        def line = []
        new File(filename).withReader {
            line = it.readLines()
        }

        //loop for create directory and file
        for(int i = 0; i < line.size(); i++){
            String firstcharWords = line[i]
            def firstchar = firstcharWords.charAt(0)
            def secondchar = firstcharWords.charAt(1)
            //dictionary/A/B/abysmal.txt
            def folder = new File("dictionary/"+firstchar)
            def subfolder = new File("dictionary/"+firstchar+"/"+secondchar)
            //check the directory is exist or not
            if (!folder.exists()){
                folder.mkdirs()
                subfolder.mkdirs()
            }else
            if (!subfolder.exists()){
                subfolder.mkdirs()
            }
            //set path to create file text
            def pathtofile = "dictionary/"+firstchar+"/"+secondchar+"/"+line[i]+".txt"

//            def getWords = "dictionary/"+line[i] + ".txt"
            //create file with words name and write 100 times word in file
            def filetext = new File(pathtofile)
            String longwords = ""
            for (int j = 0; j < 100; j++) {
                longwords += line[i]+"\n"
            }
            filetext.write(longwords)
//            break
        }

    }

    public zipFile(){
        def list = []
        def dir = new File("dictionary/")
        dir.eachFile(FileType.DIRECTORIES){ file ->
            list << file
        }
        for (int i = 0; i < list.size() ; i++) {
            def ant = new AntBuilder()
            String currentfolder = list[i]
            ant.zip(
                    destfile: currentfolder+".zip",
                    basedir: currentfolder,
                    level: 9,
//                excludes: "**/file1.dat, **/file2.dat, **/file3.dat"
            )
        }
    }

}
