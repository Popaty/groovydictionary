import com.itextpdf.text.Document
import com.itextpdf.text.Paragraph
import com.itextpdf.text.pdf.PdfWriter
import groovy.io.FileType

import java.text.DecimalFormat

/**
 * Created by Maximiliano on 8/9/15 AD.
 */
class ReportDirectory {

//    public reportSizeDirectory(pathdirectory){
//        def folder = new File("report/")
//        folder.mkdirs()
//
//        def list = []
////        def dir = new File("dictionary/")
//        def dir = new File(pathdirectory)
//        dir.eachFile(FileType.DIRECTORIES){ file ->
//            list << file
//        }
//        String result = ""
//        for (int i = 0; i < list.size() ; i++) {
//            String currentfile = list[i]
//            def currentfolder = new File(currentfile)
//
//            def pattern = "###.##K"
//            def decimalform = new DecimalFormat(pattern)
//            def foldersize =  currentfolder.directorySize() / 1024
//            currentfolder.directory
//            String getnamedirectory = currentfile.substring(currentfile.lastIndexOf("/")+1)
//            result += getnamedirectory.toUpperCase() + "              " +decimalform.format(foldersize)+"\n"
//        }
//        def reportsize = new File("report/report_size.txt")
//        reportsize.write("WELCOME \n")
//        reportsize << "Directory      size \n"
//        reportsize << result
//    }

    public reportSizeDirectory(pathdirectory){
        def folder = new File("report/")
        folder.mkdirs()

        def list = []
//        def dir = new File("dictionary/")
        def dir = new File(pathdirectory)
        dir.eachFile(FileType.DIRECTORIES){ file ->
            list << file
        }
        String result = ""
        for (int i = 0; i < list.size() ; i++) {
            String currentfile = list[i]
            def currentfolder = new File(currentfile)
            // define variable to be type "Double" to use "round" method for decimal number
            Double foldersize =  currentfolder.directorySize() / 1024
            String getnamedirectory = currentfile.substring(currentfile.lastIndexOf("/")+1)
            result += getnamedirectory.toUpperCase() + "              " +foldersize.round(2)+"\n"
        }
        def reportsize = new File("report/report_size.txt")
        reportsize.write("WELCOME \n")
        reportsize << "Directory      size \n"
        reportsize << result
    }

    public reportCompareSize(pathdirectory){
        def list = []
//        def dir = new File("dictionary/")
        def dir = new File(pathdirectory)
        dir.eachFile(FileType.DIRECTORIES){ file ->
            list << file
        }
        String result = ""
        for (int i = 0; i < list.size(); i++) {
            String currentfile = list[i]
            String getnamedirectory = currentfile.substring(currentfile.lastIndexOf("/")+1)

            def currentfolder = new File(currentfile)
            Double getsizeOriginal = currentfolder.directorySize() / 1024
            def zipcurrentfolder = new File(currentfile+".zip")
            Double getsizezip = zipcurrentfolder.length() / 1024
//            println("getsizeOriginal "+getsizeOriginal)
//            println("getsizezip "+getsizezip)

            Double reducePercentage = 100 - ((100*getsizezip)/getsizeOriginal)
//            print(reducePercentage)
//            def pattern = "###.##"
//            def decimalform = new DecimalFormat(pattern)
            result += getnamedirectory.toUpperCase()+"                  "+getsizeOriginal.round(2)+"K"+"          "+getsizezip.round(2)+"K"+"         "+reducePercentage.round(2)+"%"+"\n"
//            break

        }

        def reportdirectoryzip = new File("report/report_compare_size.txt")
        reportdirectoryzip.write("Welcome\n")
        reportdirectoryzip << "Directory    original size    zipped size    reduce in percent\n"
        reportdirectoryzip << result

    }

    public exportToPDF(){
//        def sql = Sql.newInstance("jdbc:hsqldb:file:datasource/dictionary","SA", "","org.hsqldb.jdbcDriver")
        DictionaryDatabase dd = new DictionaryDatabase()
        def sql = dd.connectionDB()
        def list = []
        sql.rows("SELECT * FROM WORDS").each {row ->
            println("words [ ${row.id}, ${row.vocabulary} ]")
            list += "${row.vocabulary}"
        }

        def document = new Document()
        PdfWriter.getInstance(document, new FileOutputStream("report/ExportReportDB.pdf"))
        document.open()
        //write word in list to file
        for (int i = 0; i < list.size(); i++) {
            String currentword = list[i]
            document.add(new Paragraph(currentword))
        }
        document.close()
    }
}
