import javax.swing.JButton
import javax.swing.JFrame
import javax.swing.JTextArea
import javax.swing.SwingUtilities
import java.awt.Dimension
import java.awt.GridLayout
import java.awt.event.ActionEvent
import java.awt.event.ActionListener

/**
 * Created by Maximiliano on 8/8/15 AD.
 */
class Main {
//    public static void main(String[] args) {
//        DictionaryDatabase dd = new DictionaryDatabase()
//        try{
//            dd.deleteDictionary()
//            println("Success")
//        }catch (Exception e){
//            println("Fail")
//        }
//
//    }
    public static void main(String[] args) {
        SwingUtilities.invokeLater( new Runnable() {
            public void run() {
                JFrame frame = new JFrame("Dictionary");
                final JTextArea textArea = new JTextArea()
                JButton button = new JButton("Start");
                button.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent event) {
                        FileDictionary fd = new FileDictionary()
                        ReportDirectory rd = new ReportDirectory()
                        DictionaryDatabase dd = new DictionaryDatabase()
                        String summaryresult = ""

                        //clear database
                        dd.deleteDictionary()

                        //create file and directory
                        try{
                            fd.createFile("words(20000).txt")
                            summaryresult += "Create files and Directory : Success\n"
                        }catch (Exception e){
                            summaryresult += "Create files and Directory : Fail\n"
                        }


                        //report directory
                        try {
                            rd.reportSizeDirectory("dictionary/")
                            summaryresult += "Create Report Directory : Success\n"
                        }
                        catch (Exception e){
                            summaryresult += "Create Report Directory : Fail\n"
                        }


                        //zip directory
                        try {
                            fd.zipFile()
                            summaryresult += "Zip Directory : Success\n"
                        }catch (Exception e){
                            summaryresult += "Zip Directory : Fail\n"
                        }


                        //Report compare between file zip and directory
                        try{
                            rd.reportCompareSize("dictionary/")
                            summaryresult += "Report Compare file zip and directory : Success\n"
                        }catch (Exception e){
                            summaryresult += "Report Compare file zip and directory : Fail\n"
                        }


                        //create table "words" in database
                        try{
                            dd.createtable()
                            summaryresult += "Create table in database : Success\n"
                        }catch (Exception e){
                            summaryresult += "Create table in database : Fail\n"
                        }


                        //Insert word to database
                        try{
                            dd.inserttodb("words(20000).txt")
                            summaryresult += "Insert words to database : Success\n"
                        }catch (Exception e){
                            summaryresult += "Insert words to database : Fail\n"
                        }


                        //print word length more than 5
                        def result = dd.findWordLengthMoreThan5()
                        summaryresult += "Find words which length more than 5 : "+result+"\n"


                        def result1 = dd.findOccurenceChar2()
                        summaryresult += "Find words which have occurence charactor >= 2 : "+result1+"\n"


                        def result2 = dd.findFirstandLastChar()
                        summaryresult += "Find words which same First and Last character : "+result2+"\n"


                        //update word in database Change first Character to Uppercase
                        try{
                            dd.updateUppercase()
                            summaryresult += "Update first chracter to Uppercase : Success\n"
                        }catch (Exception e){
                            summaryresult += "Update first chracter to Uppercase : Fail\n"
                        }


                        //write report that export from database
                        try{
                            rd.exportToPDF()
                            summaryresult += "Export words in DB to PDF : Success\n"
                        }catch (Exception e){
                            summaryresult += "Export words in DB to PDF : Fail\n"
                        }



                        def summary = new File("report/summary.txt")
                        summary.write(summaryresult)
                        print(summaryresult)

                        textArea.setText(summaryresult)
                    }
                });

                frame.setPreferredSize(new Dimension(800,800))
                frame.getContentPane().setLayout(new GridLayout(2,0));
                frame.getContentPane().add(textArea);
                frame.getContentPane().add(button);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.pack();
                frame.setVisible(true);
            }
        } );
    }



}
